# -*- coding: utf-8 -*-
"""A module for querying the PhraseFinder web service.

This module provides routines for searching the Google Books Ngram Dataset
(version 2, <http://storage.googleapis.com/books/ngrams/books/datasetsv2.html>)
by querying PhraseFinder's web API; see <https://phrasefinder.io/api>.

License: Apache 2.0
"""

from enum import Enum, unique
import urllib
import requests

_BASE_URL = 'https://api.phrasefinder.io/search'
_VERSION = '0.11.0'  # Sync with setup.py

_ESCAPE_SEQUENCES = str.maketrans({
    '?': '\\?',
    '*': '\\*',
    '/': '\\/',
    '~': '\\~',
    '"': '\\"',
    '\\': '\\\\'
})


@unique
class Corpus(Enum):
    """Defines numeric constants that represent available corpora.

    The Google Books Ngram Dataset is devided into several corpora that contain
    ngrams in different languages. See section "Corpora" under
    <https://books.google.com/ngrams/info>.

    Constants:
        AMERICAN_ENGLISH: Books predominantly in the English language that were
            published in the United States.
        BRITISH_ENGLISH: Books predominantly in the English language that were
            published in Great Britain.
        CHINESE: Books predominantly in simplified Chinese script.
        FRENCH: Books predominantly in the French language.
        GERMAN: Books predominantly in the German language.
        RUSSIAN: Books predominantly in the Russian language.
        SPANISH: Books predominantly in the Spanish language.
    """

    AMERICAN_ENGLISH = 1
    BRITISH_ENGLISH = 2
    CHINESE = 3
    FRENCH = 4
    GERMAN = 5
    RUSSIAN = 6
    SPANISH = 7

    def __str__(self):
        return self.short_name()

    def short_name(self) -> str:
        """Returns the short name of this enum constant."""
        return {
            1: 'eng-us',
            2: 'eng-gb',
            3: 'chi',
            4: 'fre',
            5: 'ger',
            6: 'rus',
            7: 'spa'
        }[self.value]


class Term():  # pylint: disable=too-few-public-methods
    """Represents a single term (word, punctuation mark, etc.) as part of a
    phrase.

    Attributes:
        tag: A `Tag` constant indicating the role of the term.
        text: A string with the term's textual content.
    """

    def __init__(self):
        self.tag = Term.Tag.GIVEN
        self.text = ''

    @unique
    class Tag(Enum):
        """Defines numeric constants that represent the role of a term with
        respect to a query.

        Constants:
            GIVEN: Term that was given as part of the query.
            INSERTED: Term that has been inserted either by an application of
                the "?" or the "*" operator.
            ALTERNATIVE: Term that was an alternative in form of the left- or
                right-hand side of the "/" operator.
            COMPLETED: Term that has been completed by an application of the
                "~" operator.
        """

        GIVEN = 0
        INSERTED = 1
        ALTERNATIVE = 2
        COMPLETED = 3


class Phrase():  # pylint: disable=too-few-public-methods
    """Represents a phrase, a.k.a. n-gram or ngrams.

    A phrase consists of a sequence of terms and associated metadata.

    Attributes:
        terms: List of Term objects.
        match_count: Total number of occurrences (absolute frequency).
        volume_count: Total number of books the phrase appears in.
        first_year: First year of publication of a book the phrase appears in.
        last_year: Last year of publication of a book the phrase appears in.
        score: The relative frequency based on the result set's mass.
        id: A numeric identifier which is unique among all phrases.
    """

    def __init__(self, line=None):
        self.terms = []
        self.match_count = 0
        self.volume_count = 0
        self.first_year = 0
        self.last_year = 0
        self.score = 0.0
        self.id = 0  # pylint: disable=invalid-name
        if line:
            self._parse(line)

    def _parse(self, line: str):
        """Parses a phrase from a tab-separated line of text.

        Args:
            line: a string that contains a TSV-encoded phrase.
        """
        parts = line.split('\t')
        assert len(parts) == 7, 'Invalid TSV format: {}'.format(line)
        for tagged_term in parts[0].split(' '):
            term = Term()
            term.tag = int(tagged_term[-1])
            term.text = tagged_term[:-2]
            self.terms.append(term)
        self.match_count = int(parts[1])
        self.volume_count = int(parts[2])
        self.first_year = int(parts[3])
        self.last_year = int(parts[4])
        self.id = int(parts[5])
        self.score = float(parts[6])


class SearchOptions():  # pylint: disable=too-few-public-methods
    """Represents optional parameters that may be sent along with a query.

    These parameters can be used to filter the result set. For example, a query
    like "hello *" would normally match ngrams that are 1 to 5 terms long. In
    order to reduce this set to contain only 2- and 3-grams, set `nmin = 2` and
    `nmax = 3`. To further reduce the set to the top ten phrases (based on
    their `match_count` attribute), set `topk = 10`.

    Constants:
        DEFAULT_NMIN: The default minimum phrase length (number of terms).
        DEFAULT_NMAX: The default maximum phrase length (number of terms).
        DEFAULT_TOPK: The default number of phrases to return. This is also the
            upper hard limit. Requests with a larger `topk` parameter will be
            rejected by returning a "400 Bad Request" response.

    Attributes:
        nmin: The minimum phrase length (number of terms).
        nmax: The maximum phrase length (number of terms).
        topk: The maximum number of phrases to return. Lower values might lower
            overall response time.
    """

    DEFAULT_NMIN = 1
    DEFAULT_NMAX = 5
    DEFAULT_TOPK = 100

    def __init__(self):
        self.nmin = SearchOptions.DEFAULT_NMIN
        self.nmax = SearchOptions.DEFAULT_NMAX
        self.topk = SearchOptions.DEFAULT_TOPK


class SearchResult():  # pylint: disable=too-few-public-methods
    """Represents the result of a search request.

    Attributes:
        error: Set to `None`, if the result was successful. Otherwise it is a
            dict that contains `code` and `message` keys and associated values.
        phrases: A list of `Phrase` objects. Successful requests may return an
            empty list if no matching phrases were found.
    """

    def __init__(self, error=None):
        self.error = error
        self.phrases = []


def search(corpus, query, options=SearchOptions()) -> SearchResult:
    """Sends a request to the server.

    Args:
        corpus: A `Corpus` constant selecting the corpus to be searched.
        query: The backslash-escaped query string. See `escape_query_term`.
        options: A `SearchOptions` object for optional parameters.

    Returns:
        A `SearchResult` object.
    """
    params = {
        'corpus': corpus.short_name(),
        'query': urllib.parse.quote(query),
        'format': 'tsv'
    }
    if options.nmin != SearchOptions.DEFAULT_NMIN:
        params['nmin'] = options.nmin
    if options.nmax != SearchOptions.DEFAULT_NMAX:
        params['nmax'] = options.nmax
    if options.topk != SearchOptions.DEFAULT_TOPK:
        params['topk'] = options.topk
    params_str = '&'.join(
        '{}={}'.format(name, value) for name, value in params.items())
    url = '{}?{}'.format(_BASE_URL, params_str)
    headers = {'User-Agent': 'phrasefinder-client-python/' + _VERSION}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:  # OK
        result = SearchResult()
        for line in response.iter_lines(decode_unicode=True):
            result.phrases.append(Phrase(line))
        return result
    if response.status_code == 400:  # Bad Request
        return SearchResult(response.json()['error'])

    raise AssertionError('Unexpected HTTP status code: {}'.format(
        response.status_code))


def escape_query_term(term: str) -> str:
    """Returns a backslash-escaped version of the input string.

    A query usually contains operators or wildcards that must be escaped when
    used literally in a query. For example, an unescaped question mark "?" is
    treated as a placeholder for a term to be inserted, while the backslash-
    escaped version "\\?" is treated as a regular character. Not escaping query
    terms may change the semantic of a query or even leads to invalid queries.
    See <https://phrasefinder.io/documentation#escape-sequences> for more
    information.

    This function should be applied to individual query terms and not to an
    entire query as contained operators and wildcards will become ineffective.

    Args:
        term: The string to be escaped.
    """
    return term.translate(_ESCAPE_SEQUENCES)
