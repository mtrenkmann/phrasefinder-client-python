# PhraseFinder Python Client

[PhraseFinder](https://phrasefinder.io) is a search engine for the [Google Books Ngram Dataset](http://storage.googleapis.com/books/ngrams/books/datasetsv2.html) (version 2). This repository contains the official Python client for requesting PhraseFinder's web [API](https://phrasefinder.io/api) which is free to use for any purpose.

* [Python API Documentation](https://mtrenkmann.gitlab.io/phrasefinder-client-python/)
* [Web API Documentation](https://phrasefinder.io/documentation)

## Installation

The library requires Python >= 3.5.

```sh
pip3 install git+https://gitlab.com/mtrenkmann/phrasefinder-client-python.git
```

*Linux users might use `sudo`.*

## Updating

```sh
pip3 install git+https://gitlab.com/mtrenkmann/phrasefinder-client-python.git --upgrade
```

*Linux users might use `sudo`.*

## Example usage

```python
from phrasefinder import phrasefinder as pf

def main():
    """
    Performs a simple request and prints out the result.
    """

    # Set up your query.
    query = 'I like ???'

    # Optional: set the maximum number of phrases to return.
    options = pf.SearchOptions()
    options.topk = 10

    # Send the request.
    try:
        result = pf.search(pf.Corpus.AMERICAN_ENGLISH, query, options)
        if result.error:
            print('Request was not successful: {}'.format(result.error['message']))
            return

        # Print phrases line by line.
        for phrase in result.phrases:
            print('{0:6f}'.format(phrase.score), end='')
            for term in phrase.terms:
                print(' {}'.format(term.text), end='')
            print()

    except Exception as error:
        print('Fatal error: {}'.format(error))

if __name__ == '__main__':
    main()
```

### Run

```sh
git clone https://gitlab.com/mtrenkmann/phrasefinder-client-python.git
cd phrasefinder-client-python

# If the package is already installed:
python3 examples/simple_request.py

# If the package is not installed:
PYTHONPATH=. python3 examples/simple_request.py
```

*There are various methods setting PYTHONPATH. This one works on Linux.*

### Output

```
0.175468 I like to think of
0.165350 I like to think that
0.149246 I like it . "
0.104326 I like it , "
0.091746 I like the way you
0.082627 I like the idea of
0.064459 I like that . "
0.057900 I like it very much
0.055201 I like you . "
0.053677 I like the sound of
```
