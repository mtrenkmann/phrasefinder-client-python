from setuptools import setup

setup(
   name='phrasefinder',
   version='0.11.0',
   description='A search engine for the Google Books Ngram Dataset',
   url='https://phrasefinder.io',
   author='Martin Trenkmann',
   author_email='martin@phrasefinder.io',
   license='Apache 2.0',
   packages=['phrasefinder'],
   package_dir={'phrasefinder': 'phrasefinder'},
   install_requires=['requests', 'urllib3'],
)
